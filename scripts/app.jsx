import './main.css';
import 'bootstrap';
import 'jquery';
import React, {Component} from 'react';
import {render} from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import NotFound from './components/notfound/notfound'
import { CurrentOrder } from './components/order/order';
import LoginState from './components/login/loginState';
import RegistrationForm from './components/login/registrationForm';
import SignUpVerify from './components/login/signupVerify'
import Menu from './components/menu/menu';
import * as auth from './utils/auth';
import OrderHistory from './components/order/orderHistory';
import { Link } from 'react-router';
import { StartPage } from './components/info/info'
import { CoursePage } from './components/course/course'


class App extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            order: {},
            isLoggedIn: auth.loggedIn(),
            balance: 0
        }
        this.handleLoginLogout = this.handleLoginLogout.bind(this);
    }
    handleLoginLogout(isLoggedIn) {
        this.setState({isLoggedIn: isLoggedIn});
        if (isLoggedIn) {
            browserHistory.push('/');
        }
    }
    _renderPage() {
        if (this.state.isLoggedIn) {
            return (
                <div className="fill">
                    <Menu
                        handleLoginLogout={ this.handleLoginLogout }
                        onChangeCurrentNextWeek={ this.onChangeCurrentNextWeek }
                        isLoggedIn={ this.state.isLoggedIn }
                    />
                    { this.props.children }
                    { this.props.location.pathname == '/' ? <StartPage/> : null }
                </div>
            )
        }
        else {
            if (this.props.location.pathname == '/registration') {
                return <RegistrationForm/>
            }
            else if (this.props.location.pathname == '/signup_verify') {
                return <SignUpVerify code={this.props.location.query.code}/>
            }
            return <LoginState
                    getBalance={ this.getBalance }
                    handleLoginLogout={ this.handleLoginLogout }
                    isLoggedIn={ this.state.isLoggedIn }
                   />
        }
    }
    render() {
        return (
            <div className="fill">
                    { this._renderPage() }
            </div>
        )
    }
}


render((
    <div>
        <Router history={browserHistory}>
            <Route path="/" component={App}>
                <Route path="/registration" component={RegistrationForm}/>
                <Route path="/signup_verify" component={SignUpVerify}/>
                <Route path="/login" component={LoginState} onEnter={auth.checkAuth}/>
                <Route path="/course" component={CoursePage} onEnter={auth.requireAuth}/>
                <Route path="/current_order" component={CurrentOrder} onEnter={auth.requireAuth}/>
                <Route path="/orders" component={OrderHistory} onEnter={auth.requireAuth} />
                <Route path="*" component={NotFound}/>
            </Route>
        </Router>
    </div>
), document.getElementById('app'));
