import React from 'react';
import { Link } from 'react-router';
import { LoginServices } from '../../dal/login/services'
import { LoginEmailPassword } from '../ui-components/emailPassword'
import { ErrorSuccessComponent } from '../ui-components/errorSuccess'


export default class RegistrationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            error: '',
            success: ''
        };
        this.handleRegistration = this.handleRegistration.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
    }
    _renderSuccesError() {
        return <ErrorSuccessComponent success={this.state.success} error={this.state.error}/>
    }
    handleChangeEmail(event) {
        this.setState({email: event.target.value});
    }
    handleChangePassword(event) {
        this.setState({password: event.target.value});
    }
    handleRegistration(e) {
        var self = this;
        e.preventDefault();
        LoginServices
            .signup({'email': self.state.email, 'password': self.state.password})
            .done(
                function(data, textStatus, jqXHR){
                    self.setState({success: 'Проверьте пожалуйста почту'});
                }
            ).fail(
            function( jqXHR, textStatus, errorThrown ) {
                var error = JSON.parse(jqXHR.responseText);
                if ('detail' in error){
                    error = error['detail'];
                }
                else {
                    error = error['non_field_errors'][0];
                }
                self.setState({error: error});
            });
    }
    render() {
        return (
            <div className="jumbotron">
                <div className="row text-center">
                    <div className="col-md-8 col-md-offset-2 text-center">
                        {this._renderSuccesError()}
                    </div>
                    <div className="col-md-12 text-center">
                        <form className="form-signin">
                            <h3 className="form-signin-heading">Регистрация</h3>
                            <LoginEmailPassword onChangeEmail={this.handleChangeEmail} onChangePassword={this.handleChangePassword}/>
                            <Link to='/login'>
                                <button className="btn btn-lg btn-primary btn-block" type="submit" onClick={this.handleRegistration}>
                                    Зарегистрироваться
                                </button>
                            </Link>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
