import React from 'react';
import LoginForm from './loginForm';


export default class LoginState extends React.Component {
    render() {
        const isLoggedIn = this.props.isLoggedIn;
        return (
            <div className="loginState">
                {!isLoggedIn ? (<LoginForm handleLoginLogout={this.props.handleLoginLogout}/>): null}
            </div>
        )
    }
}
