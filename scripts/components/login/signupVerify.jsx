import React from 'react';
import { Link } from 'react-router';
import { LoginServices } from '../../dal/login/services'
import { ErrorSuccessComponent } from '../ui-components/errorSuccess'


export default class SignUpVerify extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            success: '',
            error: '',
        };
        this.handleRegistration = this.handleRegistration.bind(this);
    }
    handleErrorSuccess(){
        return <ErrorSuccessComponent success={this.state.success} error={this.state.error}/>
    }
    componentWillMount(){
        if (this.props.code){
            this.handleRegistration(this.props.code);
        }
    }
    handleRegistration(code) {
        var self = this;
        LoginServices
            .signup_verify({code:code})
            .done(
                function(data, textStatus, jqXHR){
                    self.setState({success: data['success']});
                }
            ).fail(
            function( jqXHR, textStatus, errorThrown ) {
                self.setState({error: JSON.parse(jqXHR.responseText)['detail']});
            });
    }
    render() {
        return (
            <div className="container">
                <div className="row top-buffer">
                    {this.handleErrorSuccess()}
                </div>
                <div className="row">
                    <div className="col-md-12 col-sm-12 text-center">
                        <h3 className="form-signin-heading">Проверка почты</h3>
                    </div>
                </div>
                <div className="row top-buffer">
                    <div className="col-md-offset-4 col-sm-offset-4 col-md-4 col-sm-4">
                        <Link to="/login">
                            <button className="btn btn-lg btn-success btn-block">
                                Войти
                            </button>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}
