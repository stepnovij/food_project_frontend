import React from 'react';
import { LoginServices } from '../../dal/login/services'
import { LoginEmailPassword } from '../ui-components/emailPassword'
import * as auth from '../../utils/auth';
import { Link } from 'react-router';



export default class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            error: '',
            isLoggedIn: false
        };
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChangeEmail(event) {
        this.setState({email: event.target.value});
    }
    handleChangePassword(event) {
        this.setState({password: event.target.value});
    }
    _renderError(data){
        var json_data = JSON.parse(data['responseText']);
        if ('non_field_errors' in json_data){
            this.setState({error: json_data['non_field_errors'][0]});
        }
        if ('detail' in json_data){
            this.setState({error: 'Почта или пароль введены неверно'});
        }
        if ('password' in json_data){
            this.setState({error: 'Пароль должен быть не менее 5 символов'});
        }
    }
    handleError(){
        if (this.state.error){
            return (
                <div className="row">
                    <div className="col-md-12 col-sm-12 alert alert-danger text-center">
                        <h4>{ this.state.error }</h4>
                    </div>
                </div>
            )
        }
    }
    // componentWillReceiveProps(nextProps){
    //     nextProps.handleLoginLogout(auth.loggedIn());
    // }
    handleSubmit(e) {
        var self = this;
        e.preventDefault();
        LoginServices
            .login({'email': self.state.email, 'password': self.state.password})
            .then(
                function(data, textStatus, jqXHR){
                    auth.login();
                    self.props.handleLoginLogout(auth.loggedIn());
                    // self.setState({isLoggedIn: auth.loggedIn()})
                },
                function( jqXHR, textStatus, errorThrown ) {
                    self._renderError(jqXHR);
                }
            );
    }
    render() {
        return (
            <div className="container">
                <div className="col-md-4 col-sm-8 col-md-offset-4 col-sm-offset-2">
                    <div className="row">
                        {this.handleError()}
                    </div>
                    <form className="form-signin">
                        <h3 className="form-signin-heading text-center">Заказ еды в офис</h3>
                        <LoginEmailPassword onChangeEmail={this.handleChangeEmail} onChangePassword={this.handleChangePassword}/>
                        <button className="btn btn-lg btn-primary btn-block" type="submit" onClick={this.handleSubmit}>Войти</button>
                        <Link to="/registration">
                            <input className="btn btn-lg btn-success btn-block" type="submit" value="Регистрация"/>
                        </Link>
                    </form>
                </div>
            </div>
        )
    }
}
