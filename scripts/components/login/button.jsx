import React from 'react';
import * as auth from '../../utils/auth'
import { LoginServices } from '../../dal/login/services'


export default class LogoutButton extends React.Component {
    constructor(props) {
        super(props);
        this.handleLogoutClick = this.handleLogoutClick.bind(this);
    }
    handleLogoutClick() {
        var self = this;
        LoginServices.logout().done(function(data, textStatus, jqXHR){
            auth.logout();
            self.props.handleLoginLogout(auth.loggedIn());
        }).fail(
            function( jqXHR, textStatus, errorThrown ) {
                self._renderError(jqXHR);
        });
    }
    render() {
        return (
            <a className="logoutButton" onClick={this.handleLogoutClick}>
                Выйти
            </a>
        );
    }
}
