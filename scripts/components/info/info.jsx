import React, {Component} from 'react';
import { Link } from 'react-router';


export class StartPage extends React.Component {
    getCurrentWeekMenuActive() {
        var today = new Date();
        var todayWeekDay = today.getDay();
        if (todayWeekDay > 0 && todayWeekDay < 5) {
            return (
                <Link to="/course?week=current">
                    <input type="button" className="btn btn-success col-xs-12" value="Заказать на эту неделю"/>
                </Link>
            )
        }
        else {
            return (
                <input type="button" className="btn btn-success col-xs-12 disabled" disabled value="Заказать на эту неделю"/>
            )
        }
    }
    getNextWeekMenuActive() {
        var today = new Date();
        var todayWeekDay = today.getDay();
        if (todayWeekDay >= 5 || todayWeekDay == 0) {
            return (
                <Link to="/course?week=next">
                    <input type="button" className="btn btn-success col-xs-12" value="Заказать на следующую неделю"/>
                </Link>
            )
        }
        else {
            return (
                <input type="button" className="btn btn-success col-xs-12 disabled" disabled value="Заказать на следующую неделю"/>
            )
        }
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12 col-sm-12"><h4>Заказ обедов</h4></div>
                </div>
                <div className="row">
                    <div className="col-md-12 col-sm-12 text-left">
                        <ul>
                            <li>Еда заказывается сегодня на следующие дни.
                                Заказ еды на завтра нужно осуществить сегодня до 14.50.
                                Еда предварительно оплачивается.</li>

                            <li>Оплата производится двумя способами, наличным и безналичным расчетом.
                                <ul><li>Оплата переводится на карту 5211 7826 7837 5822 (alfa_bank)
                                    Желательно подписать перевод, например «Иванов» .</li>
                                    <li>Для оплаты наличными можно связаться «d.zakharov» по средствам «slack» или почты.</li>
                                </ul>
                            </li>

                            <li>Еда приезжает в офис в промежутке с 12.55 до 13.30.
                                Найти свою еду можно на кухне 4.4.
                                По всем вопросам, связанным с заказом еды, можно обращаться в slack-канал <a href="https://ramblercoteam.slack.com/messages/food_in_office/">#food_in_office</a>
                            </li>

                            <li>Для заказа еды выберите нужную неделю.
                                В списке, для нужного дня, проставьте кол-во напротив интересующих позиций.
                                Нажмите кнопку «Заказать».
                                Подтвердите заказ.
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="row top-buffer">
                    <div className="col-md-4 col-sm-4 col-xs-12 text-left">
                        {this.getCurrentWeekMenuActive()}
                    </div>
                </div>
                <div className="row top-buffer">
                    <div className="col-md-4 col-sm-4 col-xs-12 text-left">
                        {this.getNextWeekMenuActive()}
                    </div>
                </div>
            </div>
        )
    }
}


