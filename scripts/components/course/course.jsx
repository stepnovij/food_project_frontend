import React from 'react';
import Collapse, { Panel } from 'rc-collapse';
import { BuyButton, OrderBox } from '../order/order';
import { CourseServices } from '../../dal/course/services'



class CourseDayRow extends React.Component {
    render() {
        return <div className="row"><h3>{this.props.weekday}</h3></div>
    }
}

class CourseCategoryRow  extends React.Component {
    render() {
        return <div className="row"><h4>{this.props.courseType}</h4></div>
    }
}

class CourseRow extends React.Component{
    constructor(props) {
        super(props);
        this.state = {isChecked: false};
        this.onChange = this.onChange.bind(this);
    }
    onChange(event) {
        this.setState({value: event.target.value});
        this.props.changeOrder(this.props.course, event.target.value);
    }
    render() {
        return (
            <div className="row top-buffer">
                <div className="col-md-7 col-sm-6 col-xs-5 text-left">{this.props.course.name}</div>
                <div className="col-md-2 col-sm-2 col-xs-2">{this.props.course.weight}</div>
                <div className="col-md-2 col-sm-2 col-xs-2">{this.props.course.price}</div>
                <div className="col-md-1 col-sm-2 col-xs-2">
                    <select className="form-control col-sm-1" value={this.state.value} onChange={this.onChange}>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>
        )
    }
}

class CourseHeader extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="row top-down-buffer">
                <div className="col-md-7 col-sm-6 col-xs-5 text-left"><b>Наименование</b></div>
                <div className="col-md-2 col-sm-2 col-xs-2"><b>Вес, г</b></div>
                <div className="col-md-2 col-sm-2 col-xs-2"><b>Цена, руб</b></div>
                <div className="col-md-1 col-sm-2 col-xs-2"><b>Количество</b></div>
            </div>
        )
    }
}

class CourseTable extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            activeKey: null,
            innerActiveKey: null,
            error: false
        };
        this.getItem = this.getItem.bind(this);
        this.getTypeItems = this.getTypeItems.bind(this);
        this.getWeekDayItems = this.getWeekDayItems.bind(this);
        this.handleChange = this.handleChange.bind(this)
        this.handleInnerChange = this.handleInnerChange.bind(this);
    }
    getItem(weekday, courseType){
        var result = [];
        var courseDays = this.props.data[0];
        result.push(<CourseHeader key={weekday}/>);
        courseDays[weekday][courseType].forEach((course) => {
            result.push(
                <CourseRow
                    course={course}
                    key={course.id}
                    changeOrder={this.props.changeOrder}
                />
            );
        });
        return result
    }
    getTypeItems(weekday, courseDays){
        var index = 0;
        var result = [];
        for (var courseType in courseDays[weekday]){
            var weekdayIndex = weekday + '_' + index;
            var courseCategoryRow = <CourseCategoryRow key={weekdayIndex} courseType={courseType} className="top-buffer"/>;
            result.push(
                <Panel header={courseCategoryRow} key={weekdayIndex}>
                    <div className="col-md-12">
                        {this.getItem(weekday, courseType)}
                    </div>
                </Panel>
            );
            index += 1;
        }
        return result
    }
    handleChange(activeKey) {
        this.setState({activeKey: activeKey});
    }
    // TODO: дубликат, придумать как избавиться:
    handleInnerChange(activeKey) {
        this.setState({innerActiveKey: activeKey});
    }
    getWeekDayItems(courseDays) {
        var result = [];
        for (var weekday in courseDays) {
            var courseDayRow = <CourseDayRow weekday={weekday} key={weekday}/>;
            result.push(
                <Panel header={courseDayRow} key={weekday}>
                    <Collapse
                        accordion={true}
                        onChange={this.handleInnerChange}
                        activeKey={this.state.innerActiveKey}
                    >{this.getTypeItems(weekday, courseDays)}
                    </Collapse>
                </Panel>
            )
        }
        return result
    }

    render() {
        var courseDays = this.props.data[0];
        var courseType = this.props.data[1];
        var week = this.props.week;
        return (
            <div>
                <div className="row">
                    {
                        this.state.error ?
                            <div className="col-md-12 col-sm-12 text-center alert alert-danger" role="alert">
                                Вы не добавили ни одного блюда
                            </div>
                            : null
                    }
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div><h1>Меню на { this.props.current_week ? ' эту ': ' следующую ' }неделю</h1></div>
                        <div className="col-md-12">
                            <Collapse
                                accordion={true}
                                onChange={this.handleChange}
                                activeKey={this.state.activeKey}
                            >{this.getWeekDayItems(courseDays)}
                            </Collapse>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


class CourseBox extends React.Component {
    constructor(props, context)  {
        super(props, context);
        this.state = {
            data: [],
        };
    }
    loadCoursesFromServer(week) {
        var self = this;
        CourseServices
            .getWeekCourse(week)
                .then(function(data){
                    self.setState({data: data});
            });
    }
    componentDidMount(){
        this.loadCoursesFromServer(this.props.week);
    }
    componentWillReceiveProps(nextProps){
        this.loadCoursesFromServer(nextProps.week);
    }
    render() {
        return (
            <div className="courseBox text-center">
                <CourseTable
                    data={this.state.data}
                    changeOrder={this.props.changeOrder}
                    current_week={this.props.week == 'current'}
                />
            </div>
        );
    }
}


export class CoursePage extends React.Component {
    constructor(props, context){
        super(props, context);
        this.state = {
            order: {},
        }
        this.changeOrder = this.changeOrder.bind(this);
    }
    changeOrder(course, value){
        this.setState({
            order:Object.assign(
                {}, this.state.order, {
                    [course.id]:{
                        quantity:value, course_object: course
                    }
                }
            )
        });
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="text-center col-md-12">
                        <OrderBox
                            order={this.state.order}
                        />
                        <CourseBox
                            week={this.props.location.query.week}
                            pollInterval={2000}
                            changeOrder={this.changeOrder}
                        />
                        <BuyButton
                            order={this.state.order}
                        />
                    </div>
                </div>
            </div>
        )
    }
}