import React from 'react';
import { Link } from 'react-router';
import LogoutButton from '../login/button'
import { UserServices } from '../../dal/user/services'
import * as auth from '../../utils/auth';


class HeaderLinks extends React.Component {
    render() {
        return (
            <ul role="nav" className="nav navbar-nav">
                <li className="dropdown">
                    <a href="#"
                       className="dropdown-toggle"
                       data-toggle="dropdown" role="button"
                       aria-haspopup="true"
                       aria-expanded="false">
                        Меню
                        <span className="caret"></span>
                    </a>
                    <ul className="dropdown-menu">
                        <li><Link to="/course?week=current">Меню на эту неделю</Link></li>
                        <li><Link to="/course?week=next">Меню на следующую неделю</Link></li>
                    </ul>

                </li>
                <li><Link to="/orders">
                    История заказов
                </Link>
                </li>
            </ul>
        )
    }
}

class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            info: {}
        }
        this.updateUserInfo = this.updateUserInfo.bind(this);
    }
    updateUserInfo(){
        var self = this;
        UserServices.userInfo().done(
            function(data, textStatus, jqXHR){
                self.setState({info: data[0]})
            }
        ).fail(function( jqXHR, textStatus, errorThrown ) {
            auth.logout()
        })
    }
    componentDidMount(){
        this.updateUserInfo()
    }
    _renderLoginLogoutMenu(){
        if (this.props.isLoggedIn){
            return (
                <div className="menu">
                    <nav className="navbar navbar-default">
                        <div className="container-fluid">
                            <div className="navbar-header">
                                <button type="button"
                                        className="navbar-toggle collapsed"
                                        data-toggle="collapse"
                                        data-target="#navbar"
                                        aria-expanded="false"
                                        aria-controls="navbar">
                                        <span className="sr-only">Toggle navigation</span>
                                        <span className="icon-bar"></span>
                                        <span className="icon-bar"></span>
                                        <span className="icon-bar"></span>
                                </button>
                                <Link to="/">
                                    <span className="navbar-brand">Food in office</span>
                                </Link>
                                <HeaderLinks/>
                            </div>
                            <div id="navbar" className="collapse navbar-collapse">
                            <ul className="nav navbar-nav navbar-right">
                                <li><a className="balanceState">Остаток: {this.state.info.balance} руб</a>
                                </li>
                                <li><LogoutButton handleLoginLogout={this.props.handleLoginLogout}/></li>
                            </ul>
                        </div>
                        </div>
                    </nav>
                </div>
            )
        }
    }
    render() {
        return <div>{ this._renderLoginLogoutMenu() }</div>
    }
}

export default Menu;
