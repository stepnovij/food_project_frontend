import React from 'react';


class NotFound extends React.Component {
    render() {
        <div className="container">
            <div className="row">
                <div className="col-md-12 col-sm-12">
                    <h3>404 page not found</h3>
                    <p>We are sorry but the page you are looking for does not exist.</p>
                </div>
            </div>
        </div>
    }
}
export default NotFound;
