import React from 'react';
import ReactModal from 'react-modal';
import { browserHistory, Link } from 'react-router';
import { OrderServices } from '../../dal/order/services'
import { ErrorSuccessComponent } from '../ui-components/errorSuccess'


class Payment extends React.Component {
    render() {
        var url = "https://money.yandex.ru/quickpay/shop-widget?account=410013715950847&quickpay=shop&payment-type-choice=on&mobile-payment-type-choice=on&writer=seller&targets-hint=&default-sum=";
        url = url + this.props.total_sum +  "&button-text=01&successURL=";
        url = url + '&label=' + this.props.order_id;
        url = url + '&targets=Оплата заказа №' + this.props.order_id;
        return (
            <div>
                <iframe
                    frameBorder="0"
                    data-allowtransparency="true"
                    scrolling="no"
                    src={url} width="450" height="198"
                   ></iframe>
            </div>
        )
    }
}


class OrderBox extends React.Component {
    constructor(props, context){
        super(props, context);
        this.getTotalPrice = this.getTotalPrice.bind(this);
    }
    getTotalPrice(order) {
        var sum = 0;
        var clean_order = {};
        for (var key in order) {
            sum += (order[key].course_object.price*order[key].quantity);
            if (order[key].quantity >  0){
                clean_order[key] = order[key]
            }
        }
        window.sessionStorage.setItem('order', JSON.stringify(clean_order));
        return sum
    }
    render() {
        return (
            <div className="row">
                <h4>Итого: {this.getTotalPrice(this.props.order)} руб.</h4>
            </div>
        )

    }
}


class BuyButton extends React.Component {
    constructor(props, context){
        super(props, context);
        this.state = {
            order: [],
        }
        this.checkOrder = this.checkOrder.bind(this);
    }
    checkOrder(object){
        var total = 0;
        for ( var key in object){
            total += object[key].quantity;
        }
        return !(total == 0)
    }
    _renderLink(){
        if (this.checkOrder(this.props.order)){
            return (
                <Link to="/current_order">
                    <input type="button" className="btn btn-success" value="заказать" />
                </Link>
            )
        }
        else {
            return <input type="button" className="btn btn-success disabled" value="заказать" />
        }
    }
    render() {
        return(
            <div>
                <div className="row top-down-full-buffer">
                    { this._renderLink() }
                </div>
            </div>
        )
    }
}


class CurrentOrder extends React.Component {
    constructor(props, context){
        super(props, context);
        this.state = {
            order: [],
            error: '',
            success: '',
            showSubmitButton: true,
            showModal: false,
            order_id: null
        }
        this.state.order = window.sessionStorage.getItem('order');
        this.handleOrder = this.handleOrder.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this._onSuccessFailure = this._onSuccessFailure.bind(this);
        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.getTotalPrice = this.getTotalPrice.bind(this);
    }
    handleOpenModal () {
        this.handleClick();
        this.setState({ showModal: true });
    }
    handleCloseModal(){
        this.setState({ showModal: false });
    }
    getTotalPrice(order) {
        var orders = JSON.parse(order)
        var total_price = 0;
        for (var key in orders) {
            if (orders[key].quantity == 0) {
                continue;
            }
            total_price += orders[key].course_object.price * orders[key].quantity;
        }
        return total_price
    }
    handleOrder(order){
        var daysMapping = {
            'Понедельник': 1,
            'Вторник': 2,
            'Среда': 3,
            'Четверг': 4,
            'Пятница': 5,
        }
        var result = [];
        var weekday = {};
        var orders = JSON.parse(order);
        var total_price = 0;
        for (var key in orders){
            if (orders[key].quantity == 0){
                continue;
            }
            if (!(daysMapping[orders[key].course_object.weekday] in weekday)){
                weekday[daysMapping[orders[key].course_object.weekday]] = [];
                weekday[daysMapping[orders[key].course_object.weekday]].push(
                    <div className="row top-down-buffer">
                        <div className="col-md-12 col-sm-12 text-left">
                            <h4>{ orders[key].course_object.weekday } ({orders[key].course_object.date})</h4>
                        </div>
                    </div>
                );
                weekday[daysMapping[orders[key].course_object.weekday]].push(
                    <div key='header' className="row">
                        <b>
                            <div className="col-md-4 col-sm-4 col-xs-4 text-left">Наименование</div>
                            <div className="col-md-4 col-sm-4 col-xs-4 text-right">Количество</div>
                            <div className="col-md-4 col-sm-4 col-xs-4 text-right">Стоимость</div>
                        </b>
                    </div>
                )
            }
            total_price += orders[key].course_object.price*orders[key].quantity;
            weekday[daysMapping[orders[key].course_object.weekday]].push(
                <div key={key} className="row">
                    <div className="col-md-4 col-sm-4 col-xs-4 text-left">{orders[key].course_object.name}</div>
                    <div className="col-md-4 col-sm-4 col-xs-4 text-right">{orders[key].quantity}</div>
                    <div className="col-md-4 col-sm-4 col-xs-4 text-right">{orders[key].course_object.price} руб.</div>
                </div>
            )
        }
        Object.keys(weekday)
            .sort()
            .forEach(function(v, i) {
                result.push(weekday[v]);
            });
        result.push(
            <div key="total" className="row top-down-buffer">
                <div className="col-md-12 col-sm-12 text-right">
                    <h4>Итого: { total_price } руб.</h4>
                </div>
            </div>
        )
    return result;
    }
    _onSuccessFailure(){
        return <ErrorSuccessComponent success={this.state.success} error={this.state.error}/>
    }
    handleClick(){
        var data = {};
        data['course'] = [];
        data['week'] = {};
        data['week']['id'] = {};
        data['quantity'] = [];
        var order = JSON.parse(this.state.order);
        Object.keys(order).map(function(key, index){
            data['course'].push(key);
            data['quantity'].push(order[key].quantity);
        });
        data['week']['id'] = order[data['course'][0]]['course_object']['week']['id'];
        data['is_payed'] = false;

        var self = this;
        OrderServices
            .create(data)
            .done(
                function(data, textStatus, jqXHR){
                    self.setState({order_id: data.id});
                    self.setState({success:'Ваш заказ успешно создан'});
                    self.setState({showSubmitButton:false});
                }
            ).fail(
            function( jqXHR, textStatus, errorThrown ) {
                self.setState({error:'Ваш заказ не создан'});
            });
    }
    render() {
        var customStyle = {content:
            {width: '490px', height: '300px', left:0, right: 0, top: 0, bottom: 0,
                'border': '0px',
                marginLeft:'auto', marginRight: 'auto', marginTop:'auto', marginBottom: 'auto'
            }
        };
        return (
            <div className="container">
                <ReactModal
                    isOpen={this.state.showModal}
                    contentLabel="Оплата обеда"
                    onRequestClose={this.handleCloseModal}
                    shouldCloseOnOverlayClick={true}
                    style={customStyle}
                >
                    <Payment total_sum={this.getTotalPrice(this.state.order)} order_id={this.state.order_id}/>
                </ReactModal>
                { this._onSuccessFailure() }
                <h3>Ваш заказ:</h3>
                {this.handleOrder(this.state.order)}
                { this.state.showSubmitButton ?
                    <div>
                        <div className="row">
                            <div className="col-md-6 col-sm-6 col-xs-12 text-left">
                                <input className="btn btn-danger" type="button" value="отмена" onClick={browserHistory.goBack}/>
                            </div>
                            <div className="col-md-6 col-sm-6 col-xs-12 text-right">
                                <input className="btn btn-primary" type="submit" onClick={this.handleOpenModal} value="Оплатить"/>
                                <input className="btn btn-success" type="submit" onClick={this.handleClick} value="Заказать"/>
                            </div>
                        </div>
                    </div>
                    :
                    <div className="row">
                        <div className="col-md-12 col-sm-12 text-center">
                            <Link to="/orders">
                                <input className="btn btn-lg btn-success btn-block" type="submit" value="История заказов"/>
                            </Link>
                        </div>
                    </div>
                }
            </div>

        )
    }
}


module.exports = {
    OrderBox,
    BuyButton,
    CurrentOrder
};
