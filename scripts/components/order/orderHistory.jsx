import React from 'react';
import { OrderServices } from '../../dal/order/services'


class FoodHeaderRow extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-9 col-sm-9 col-xs-8 text-left">
                        <h5>Наименование</h5>
                    </div>
                    <div className="col-md-3 col-sm-3 col-xs-4 text-left">
                        <h5>Количество</h5>
                    </div>
                </div>
            </div>
        )
    }

}


class FoodRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editMode: false,
            value: {},
            ordercourse_id: ''
        }
        this._render = this._render.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }
    componentWillReceiveProps(nextProps){
        this.setState({editMode: nextProps.editMode});
    }
    componentWillMount(){
        var ordercourse_id = this.props.course.ordercourses_id;
        this.setState({ordercourse_id: ordercourse_id})
        var quantity = this.props.course.total;
        var res = {};
        if (this.state.value) {
            res = this.state.value;
        }
        res[ordercourse_id] = quantity;
        this.setState({value: res});
    }
    handleChange(event){
        var dict = this.state.value;
        dict[this.state.ordercourse_id] = event.target.value;
        this.setState({value: dict});
        this.props.getNewQuantity(dict);

    }
    _render(){
        var ordercourse_id =  this.state.ordercourse_id;
        if (!this.state.editMode) {
            return (
                <div className="container">
                    <div className="row">
                        <div className="col-md-10 col-sm-10 col-xs-10">{this.props.course.course__name}</div>
                        <div className="col-md-2 col-sm-2 col-xs-2">{this.props.course.total}</div>
                    </div>
                </div>
            )
        }
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-10 col-sm-10 col-xs-10">{this.props.course.course__name}</div>
                    <select className="col-md-2 col-sm-2 col-xs-2" value={this.state.value[ordercourse_id]} onChange={this.handleChange}>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>
        )
    }
    render() {
        return this._render()
    }
}


class TabContent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            del: false,
            editMode: false,
            updateOrderCourse: {},
            isDataUpdated: false,
        }
        this.delCallback = this.delCallback.bind(this);
        this.IsInEditMode = this.IsInEditMode.bind(this);
        this.saveCallback = this.saveCallback.bind(this);
        this.getNewQuantity = this.getNewQuantity.bind(this);
        this.getRow = this.getRow.bind(this);
    }
    getNewQuantity(orderObject){
        var dict = Object.assign(this.state.updateOrderCourse, orderObject);
        this.setState({updateOrderCourse: dict});
    }
    saveCallback(){
        var self = this;
        var dict = this.state.updateOrderCourse;
        for (var key in this.state.updateOrderCourse){
            var data = {'quantity': dict[key]}
            var array = key.split(',');
            if (dict[key] > 0){
                OrderServices.updateOrderCourse({ordercourse_id: array[0], data: data}).done(
                    function(data){
                        self.props.isOrderUpdated(true)
                    }
                ).fail(function( jqXHR, textStatus, errorThrown ) {
                    console.log(jqXHR, textStatus, errorThrown);
                })
            }
            else {
                OrderServices.deleteOrderCourse({ordercourse_id: array[0]}).done(
                    function(data){
                        self.props.isOrderUpdated(true)
                    }
                ).fail(function( jqXHR, textStatus, errorThrown ) {
                    console.log(jqXHR, textStatus, errorThrown);
                })
            }
            if (array.length > 1){
                for (var key2 in array.slice(1)) {
                    OrderServices.deleteOrderCourse({ordercourse_id: array.slice(1)[key2]}).fail(
                        function( jqXHR, textStatus, errorThrown ) {
                        console.log(jqXHR, textStatus, errorThrown);
                    })
                }
            }
        }
    }
    delCallback(del){
        var data = this.props.content;
        var self = this;
        for (var idx in data){
            for (var idx2 in data[idx].ordercourses_id){
                var ordercourse_id = data[idx].ordercourses_id[idx2];
                OrderServices.deleteOrderCourse({ordercourse_id: ordercourse_id}).done(
                    function(data){
                        self.props.isOrderUpdated(true)
                    }
                ).fail(function( jqXHR, textStatus, errorThrown ) {
                    console.log(jqXHR, textStatus, errorThrown);
                })
            }
        }
    }
    getRow(data, editMode) {
        var rows = [];
        var index = 0;
        var total_price = 0;
        rows.push(<FoodHeaderRow key={data[0].course__date + '_header'}/>);
        var date = '';
        for (var indx in data){
            var course = data[indx];
            date = course.course__date;
            var complex_index = course.course__date + index;
            rows.push(
                <FoodRow editMode={editMode} course={course} key={complex_index} getNewQuantity={this.getNewQuantity}/>
            );
            index += 1;
            total_price += course.course__price * course.total;
        };
        rows.push(
            <div className="row text-right" key={date + '_total'}>
                <div className="col-md-10 col-sm-10 col-xs-10">
                    <h4>Итого: {total_price}</h4>
                </div>
            </div>
        );
        return rows;
    }
    IsInEditMode(mode){
        this.setState({editMode: mode});
    }
    render() {
        var today = new Date();
        var date = new Date(this.props.content[0].course__date);
        var isEditionPermitted = ((date - today + 11.5*60*60*1000)/(24*60*60*1000)) > 1;
        return (
            <div className='tab-pane fade' id={this.props.day}>
                {this.getRow(this.props.content, this.state.editMode)}
                <div className="row top-down-full-buffer">
                    <div className="col-md-12">
                        <EditOrderButton
                            delCallback={this.delCallback}
                            IsInEditMode={this.IsInEditMode}
                            editMode={this.state.editMode}
                            saveCallback={this.saveCallback}
                            isEditionPermitted={isEditionPermitted}
                        />
                    </div>
                </div>
            </div>
        )
    }
}


class TabElement extends React.Component {
    render() {
        return (
            <li><a href={'#' + this.props.day} data-toggle="tab">{this.props.day}</a></li>
        )
    }
}


class CourseDaySubMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            days: [],
            content: []
        };
    }
    componentWillReceiveProps(nextProps){
        var li_days = [];
        var tab_content = [];
        var active_tab = '';
        for (var key in nextProps.days){
            li_days.push(<TabElement key={key} day={key}/>);
            tab_content.push(<TabContent key={key} day={key} content={nextProps.days[key]} isOrderUpdated={this.props.isOrderUpdated}/>);
        }
        this.setState({days: li_days});
        this.setState({content: tab_content});
    }
    render(){
        return (
            <div className="container" id="orderSlider">
                <div className="row text-center">
                    <ul className="nav nav-tabs pagination text-center">
                            {this.state.days}
                    </ul>

                </div>
                <div className="row">
                    <div className="tab-content">{this.state.content}</div>
                </div>
            </div>
        )
    }
}

class EditOrderButton extends React.Component {
    constructor(props) {
        super(props);
        this.handleEdit = this.handleEdit.bind(this);
        this._render = this._render.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }
    handleEdit(){
        this.props.IsInEditMode(!this.props.editMode);
    }
    handleSave(){
        this.props.IsInEditMode(!this.props.editMode);
        this.props.saveCallback();
    }
    handleDelete() {
        this.props.delCallback(true);
    }
    _render(){
        var today = new Date();
        var todayWeekDay = today.getDay();
        if (this.props.isEditionPermitted){
            if (this.props.editMode){
                return (
                    <div className="col-md-4 col-sm-4">
                        <a className="btn btn-md btn-success btn-block" onClick={this.handleSave}>Сохранить</a>
                    </div>
                )
            }
            return (
                <div className="row">
                    <div className="col-md-3 col-sm-4 col-xs-6">
                        <a className="btn btn-md btn-primary btn-block col-md-4 col-sm-4" onClick={this.handleEdit}>Редактировать</a>
                    </div>
                    <div className="col-md-3 col-sm-4 col-xs-6 col-md-offset-5 col-sm-offset-4">
                        <a className="btn btn-md btn-danger btn-block" onClick={this.handleDelete}>Удалить</a>
                    </div>
                </div>
            )

        }
        return null;
    }
    render() {
        return this._render();
    }
}

export default class OrderHistory extends React.Component {
    constructor(props, context)  {
        super(props, context);
        this.state = {
            data: [],
            update: false,
        };
        this.isOrderUpdated = this.isOrderUpdated.bind(this);
    }
    loadDataFromServer() {
        var self = this;
        OrderServices
            .historyByDay().then(
                function(data){
                    self.setState({data:data});
                }
            );
    }
    componentDidMount() {
        this.loadDataFromServer();
    }
    isOrderUpdated(update){
        this.setState({update: update});
        if (this.state.update){
            this.loadDataFromServer();
        }
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12 col-sm-12 text-center">
                        <h2>История заказов</h2>
                    </div>
                </div>
                <div className="row">
                    <CourseDaySubMenu days={this.state.data} isOrderUpdated={this.isOrderUpdated}/>
                </div>
            </div>
        )
    }
}
