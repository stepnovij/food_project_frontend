import React from 'react';


class Email extends React.Component {
    render() {
        return (
            <div>
                <label className="sr-only">Email address</label>
                <input
                    id="inputEmail"
                    type="email"
                    name="email"
                    placeholder="username@rambler-co.ru"
                    onChange={this.props.onchange}
                    className="form-control"
                    required
                />
            </div>
        )
    }
}


class Password extends React.Component {
    render() {
        return (
            <div>
                <label className="sr-only">Password</label>
                <input
                    id="inputPassword"
                    type="password"
                    name="password"
                    placeholder="password"
                    onChange={this.props.onchange}
                    required
                    className="form-control"
                />
            </div>
        )
    }
}


export class LoginEmailPassword extends React.Component {
    render(){
        return (
            <div>
                <Email onchange={this.props.onChangeEmail}/>
                <Password onchange={this.props.onChangePassword}/>
            </div>
        )
    }
}
