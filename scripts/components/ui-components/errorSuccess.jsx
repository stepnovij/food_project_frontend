import React from 'react';


class ErrorSuccessComponent extends React.Component {
    constructor(props, context){
        super(props, context);
        this.state = {
            error: '',
            success: ''
        }
    }
    componentWillReceiveProps(nextProps) {
        this.setState({error: nextProps.error});
        this.setState({success: nextProps.success});
    }
    render() {
        if (this.state.error){
            return (
                <div className="col-md-12 col-sm-12 alert alert-danger text-center" role="alert">
                    {this.state.error}
                </div>
            )
        }
        else if (this.state.success){
            return (
                <div className="col-md-12 col-sm-12 alert alert-success text-center" role="alert">
                    {this.state.success}
                </div>
            )
        }
        else {
            return null
        }
    }
}

module.exports = {
    ErrorSuccessComponent
};
