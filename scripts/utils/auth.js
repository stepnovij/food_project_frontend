import { getCSRFtoken } from './cookie'


export function login() {
    localStorage.token = getCSRFtoken();
    return !!localStorage.token
}

export function logout() {
    delete localStorage.token
}

export function loggedIn() {
    return !!localStorage.token
}

export function requireAuth(nextState, replace) {
    if (!localStorage.token) {
        replace({
            pathname: '/login',
            state: { nextPathname: nextState.location.pathname }
        })
    }
}

export function checkAuth(nextState, replace) {
    if (localStorage.token && nextState.location.pathname == '/login'){
        replace({
            pathname: '/',
            state: { nextPathname: nextState.location.pathname }
        })
    }
}
