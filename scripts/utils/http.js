let $ = require('jquery');
import { csrfHeaderName, csrfCookieName, getCookie, getCSRFtoken } from '../utils/cookie'


export const Http = {
    get(params) {
        return $.ajax({
            type: 'GET',
            dataType: 'JSON',
            headers: {
                'X-CSRFToken': getCSRFtoken()
            },
            ...params
        });
    },
    post(params) {
        return $.ajax({
            url: params.url,
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            headers: {
                'X-CSRFToken': getCookie(csrfCookieName)
            },
            //     'credentials': 'same-origin',
            //     'X-CSRFToken': getCSRFtoken()
            // },
            data: JSON.stringify(params.data)
        });
    },
    put(params) {
        return $.ajax({
            url: params.url,
            type: 'PUT',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            headers: {
                'X-CSRFToken': getCookie(csrfCookieName)
            },
            data: JSON.stringify(params.data)
        });
    },
    delete(params) {
        return $.ajax({
            url: params.url,
            type: 'DELETE',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            headers: {
                'X-CSRFToken': getCookie(csrfCookieName)
            },
        });
    },
    patch(params) {
        return $.ajax({
            url: params.url,
            type: 'PATCH',
            dataType: 'JSON',
            contentType: 'application/json; charset=utf-8',
            headers: {
                'X-CSRFToken': getCookie(csrfCookieName)
            },
            data: JSON.stringify(params.data)
        });
    },
}