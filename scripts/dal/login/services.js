import { Http } from '../../utils/http';


export const LoginServices = {
    signup_verify(params) {
        return Http.get({
            url: 'api/signup/verify/?code=' + params.code,
        })
    },
    signup(data) {
        return Http.post({
            url: '/api/signup/',
            data: data
        })
    },
    login(data) {
        return Http.post({
            url: '/api/login/',
            data: data
        });
    },
    logout() {
        return Http.get({
            url: '/api/logout/',
        });
    },

};

