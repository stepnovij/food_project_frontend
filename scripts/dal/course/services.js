import { Http } from '../../utils/http';


function extractAndGroupData(data) {
    var courseDays = {};
    var courseType = {};
    data.forEach(function(course) {
        if (!(course.weekday in courseDays)) {
            courseDays[course.weekday] = {};
        }
        if (!(course.course_type in courseDays[course.weekday])) {
            courseDays[course.weekday][course.course_type] = [];
        }
        courseType[course.course_type] = 1;
        courseDays[course.weekday][course.course_type].push(course);
    });
    return [courseDays, courseType];
}


export const CourseServices = {
    getRawWeekCourse(week) {
        return Http.get({
            url: '/api/course/?week=' + week,
        })
    },
    getWeekCourse(week) {
        return Http.get({
            url: '/api/course/?week=' + week,
        }).then(function (data, textStatus, jqXHR) {
            return extractAndGroupData(data)
        }, function(jqXHR, textStatus, errorThrown) {
            console.error(url, status, err);
        })
    },
};
