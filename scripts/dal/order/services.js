import { Http } from '../../utils/http';


function getOrderDays(data) {
    var days = {};
    data.map(function(course){
        if (!days[course.course__date]){
            days[course.course__date] = [];
        }
        days[course.course__date] = days[course.course__date].concat(course);
    })
    return days;
}



export const OrderServices = {
    create(data) {
        return Http.post({
            url: '/api/order/',
            data: data
        })
    },
    historyRawByDay() {
        return Http.get({
            url: '/api/order_by_day/',
        })
    },
    historyByDay() {
        return Http.get({
            url: '/api/order_by_day/',
        }).then(function (data, textStatus, jqXHR) {
            return getOrderDays(data.data)
        }, function( jqXHR, textStatus, errorThrown ){
            console.error(url, status, err);
        })
    },
    getOrderCourse(params) {
        return Http.get({
            url: '/api/ordercourse_simple/' + params.ordercourse_id + '/',
        })
    },
    updateOrderCourse(params) {
        return Http.patch({
            url: '/api/ordercourse_simple/' + params.ordercourse_id + '/',
            data: params.data
        })
    },
    deleteOrderCourse(params) {
        return Http.delete({
            url: '/api/ordercourse_simple/'+  params.ordercourse_id + '/',
        })
    },
}