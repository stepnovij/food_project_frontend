import { Http } from '../../utils/http';


export const UserServices = {
    userInfo() {
        return Http.get({
            url: '/api/user/',
        });
    },
};
